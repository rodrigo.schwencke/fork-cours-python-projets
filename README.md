Ce fichier est à éditer lors de la première utilisation du projet.

- [ ] Éditer le fichier [mkdocs.yml](mkdocs.yml)
    - [ ] Modifier la clef `site_name:`
    - [ ] Modifier la clef `site_description:`
    - [ ] Modifier la clef `copyright:`
    - [ ] Modifier l'adresse mail de contact

Lors de la première construction de votre site, après avoir exécuté un premier pipeline, (voir ici : https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/08_tuto_fork/1_fork_projet/), **si vous avez décoché la case "utiliser un domaine unique"**, l'url est automatiquement définie par la clef `site_url`, afin de pointer vers une url de la forme `https://<username>.forge.apps.education.fr/<projet>/`.


- [ ] Éditer le fichier [index.md](docs/index.md)
    - [ ] Le personnaliser. Il s'agit de la page d'accueil de votre site.

La documentation est sur https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/


Pour signaler un souci sur le **modèle**, écrire à [forge-apps+guichet+modeles-projets-mkdocs-pyodide-review-99-issue-@phm.education.gouv.fr](forge-apps+guichet+modeles-projets-mkdocs-pyodide-review-99-issue-@phm.education.gouv.fr) ou ouvrir un ticket sur https://forge.apps.education.fr/modeles-projets/mkdocs-pyodide-review/-/issues

- [ ] Une fois toutes les modifications précédentes effectuées, éditer ce fichier README.md pour le personnaliser.

Vous pourrez y écrire par exemple l'adresse de rendu de votre propre site.

L'adresse originale du rendu de ce site modèle avant clonage est : 

[Modèle de projet avec Python](https://modeles-projets.forge.apps.education.fr/mkdocs-pyodide-review/)





